import {reactive,computed,h} from "vue"
import {Model,SymbolScope, on,fn,ctr,DEFAULT } from "./index.js"
export const gui = SymbolScope('gui')
export const cmp = SymbolScope('computed')
export const wtc = SymbolScope('watch')
export const ref = SymbolScope('vuedata')
export const inp = SymbolScope('props')
export const css = SymbolScope('style')
export const mouse = reactive({})
window.addEventListener('mousemove',(e)=>{
    const {offsetX,offsetY,x,y,screenX,screenY,clientX,clientY} = e
    Object.assign(mouse,{offsetX,offsetY,x,y,screenX,screenY,clientX,clientY})
})
function e(target,props,...childs){
    if (!props){props = {}}
    for(const prop in props){
        const handler = target[gui.props[prop]]
    }
}
window.h = h
// Model[fn.extend]({
//     [on.bindNS](cls){cls.objects = reactive({})}
// },{// instance
//     [ctr.reaInst]({inst},ctx){ctx.inst = reactive(inst)}
// })
// ...({
//     computed(tg,name,{val:cb,isProto,ctrs}){
//         if(!ctrs.computed){
//             ctrs.computed = function({inst}){
//                 for(const [name,val,tg] of cmp('in',inst)){
//                     inst[name] = computed(val.bind(inst))
//                 }    
//             }
//         }
//     }
// })
export function component(obj){
    const computed = cmp('from',obj)
    const methods = fn('from',obj)
    const watch = wtc('from',obj)
    let data = ref('from',obj)
    const props = inp('from',obj)
    const components = cnt('from',obj)
    const ret = {computed,methods,watch,props,components}
    for(const key of Object.getOwnPropertyNames(obj)){
        ret[key] = obj[key]
    }
    if(Object.keys(data).length){ret.data = ()=>({...data})}
    return ret
}
export function bind(obj,key='value',name="modelValue"){return {[`onUpdate:${name}`](v){console.log('change');obj[key] = v},[name]:obj[key]} }
// call gui render functions from template 
export function View($attrs){
    const [[name,val],...other] = Object.entries($attrs)
    const attrs = Object.fromEntries(other)
    const component = val[gui[name]];
    if(!component){return}
    return <component class={`${val[css.class]}-${name}-view`} target={val} {...attrs}/>
}
