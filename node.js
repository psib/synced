import { existsSync, readFileSync, statSync } from "fs";
import {createServer} from "http"
import { SharedFunc,curl} from "./index.js";
import { fn } from "./symbol.js";
// generate staticFiles Handler module
export function staticFiles(root=existsSync('dist')?"dist":"public",mimeFolder="synced"){
  let md = {root,
    mimes:existsSync(mimeFolder+'/mimes.json') && JSON.parse(readFileSync(mimeFolder+'/mimes.json')),
    mimeType(path){
      const ext = path.split('.').pop()
      if(!this.mimes){console.error('mimes.json is not found')}
      return this.mimes?.[ext]||'application/octet-stream'
    },
    call(ctx,{req,res}){
      const path = md.root + req.url;
      if(existsSync(path)&&statSync(path).isFile()){
        ctx.break()
        res.statusCode = 200;
        res.setHeader('Content-Type',md.mimeType(path))
        res.end(readFileSync(path))
        return true
      }

    }
  }
  return md
}
export function API(root){
  let md; return md = {
    root,
    call(ctx,{req}){
      let target = md.root;
      for(const name of req.url.split('/').filter(e=>e)){
        if(!target){break}
        target = target[curl.get](name)
      }
      if(target?.[curl.handler]){return target[curl.handler](ctx,ctx)}
      else {ctx.notFound()}
    }
  }
}
export function Default({req,res}){
  res.end(readFileSync("dist/index.html"));
}
export class Server{
  process = SharedFunc([(c,{args:[req,res]})=>{c[fn.extend]({req,res})}],{
      async getBody(){
        return new Promise((ok)=>{
          let data = ''
          this.req.on('data',(c)=>data+=c)
          this.req.on('end',()=>ok(data))
        })
      },
      notFound(msg){
        console.log('return "not fond"')
        this.res.statusCode = 404;
        this.res.end(`not found: ${msg}`)
      },
      redirect(url){
        console.log(`redirecting: ${url}`)
        this.res.writeHead(302, {'Location': url});
        this.res.end(); return true
      },
      getCookie(){
        var list = {},rc = this.res.headers.cookie;
        rc && rc.split(';').forEach(function( cookie ) {
            var parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURIComponent(parts.join('='));
        });
        return list;
      },
      setCookie(cookies,path){
        path = path?`; Path = ${path}`:''
        this.res.setHeader('Set-Cookie',Object.entries(cookies).map(([k,v])=>`${k}=${encodeURIComponent(v)}`).join('; ')+path);
        
      },
      updCookie(list={},path){
        this.setCookie(Object.assign(this.getCookie(),list),path)  
      }
  });
  constructor(host = null,port = 8000){
    this.host = host;
    this.port = port
    this.server = createServer(this.process)
  }
  run(){   
    const cb = ()=>{console.log(`Server started: ${this.host||""}:${this.port}`)}
    if(this.host){
      this.server.listen(this.port,this.host,cb)
    }else{this.server.listen(this.port,cb)}
  };
}