import {SymbolScope,fn,cfg,DEFAULT, Proxymbol,update} from "."
import { CSS } from "./css"
export const gui = new SymbolScope('gui')
export const css = new SymbolScope('css')
gui.props = new SymbolScope('gui:props')
export function e(tg,props,...childs){
    const prevDep = e.current
    if (!props){props = {}}
    e.current = props;
    props[cfg.childs] = childs
    props[cfg.target] = tg
    /**@type {HTMLElement} */
    const el  = (tg instanceof HTMLElement)? tg :tg[gui.render]?.(props,props);
    if(!el) return e.current = prevDep,null
    props[cfg.element] = el;
    for(let prop in props){
        const handler = tg[gui.props[prop]]
        const value = props[prop]
        if(handler){handler.apply(tg,[el,value,props])}
        else {
            const defHandler = tg[gui.props[DEFAULT]]
            if (defHandler){defHandler(el,prop,value,props);continue}
            if(prop.startsWith('on')){ prop = prop.toLowerCase();}
            if (prop in HTMLElement.prototype || ["object","function"].includes(typeof value)){
                el[prop] = value;
            }else{
                el.setAttribute(prop,value)
            }
        }
    }
    for(let childVal of props[cfg.childs].flat()){
        if(childVal==null){continue}
        const childEl = childVal instanceof HTMLElement?childVal:childVal[gui.child]?.(el)
        childEl && el.append(childEl)
    }
    return e.current = prevDep, el;
}
e[fn.extend]({
    updateList:new Set(),
    update:(function (node){
        this.updateList.add(node);
        !this.cbId && (this.cbId = requestAnimationFrame(()=>{
            this.cbId = null;
            for(const node of this.updateList){
                console.log(node)
                const tg = node[cfg.target]
                const childs = node[cfg.childs]
                node[cfg.element].replaceWith(h(tg,node,...childs))
            }
            this.updateList.clear();
        }));
    }).bind(e)
})
window.h = e
export function bind(tg,key){
    return {
        onchange(e){tg[key] = e.target.value},
        value:tg[key]
    }
}
export class List{
    constructor(tag="ul",values = []){
        this.tag = tag;
        this.values = values;
    }
    [gui.render]({for:cb},props){
        delete props.for
        return this.el = <this.tag>{this.values.map(this.cb = cb)}</this.tag>
    }
    update(arr){
        this.values = arr;
        if(this.cb)<this.el innerHTML="">{arr.map(this.cb)}</this.el>;
    }
}

// Extensions
String[fn.extend]({},{
    [gui.render](){return document.createElement(this)},
    [gui.child](){return this}
})
Function[fn.extend]({},{
    [gui.render](...args){return this(...args)}
})
Object[fn.extend]({},{
    /**@param {HTMLElement} el*/
    [gui.props.class](el,value,props){
        const list = value[gui.classList]?.()
        if(list && list.length){el.classList.add(...list)}
        if(typeof value == 'string'){el.classList.add(value)}
        else if (Array.isArray(value)){
            
        }
    },
    [gui.class](){return this.name}
})
Array[fn.extend]({},{
    [gui.classList](){
        return this.map((clas)=>{
            return typeof clas =='string'?clas: clas[gui.class]?.()
        }).filter(e=>e!=null&&e!="")
    }
})
Number[fn.extend]({},{
    [gui.child]:String.prototype[gui.child],
})
CSS[update]({
    [gui.render](){
        const style = Object.entries(this.dict).map(([name,css])=>`${name}{${obj2css(css)}}`).join(';\n')
        return <style textContent={style}></style>
    }
})