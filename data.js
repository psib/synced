import { computed, reactive, watch } from "vue";
import {SymbolScope,cfg,fn,SharedFunc} from ".";
export const rmt = SymbolScope('remote') // remote function fetch to server
export const fld = SymbolScope('field');
const updateQueue = {
    //TODO
}
export function LSget(key){
    const val = localStorage[key];
    return val && JSON.parse(val);
}
export class IDBStorage{
    static pkName = "id";
    static dbName = "core";
    static tableName = "test";
    static [fn.pack](val){
        return val[this.pkName]
    }   
    static async [fn.unpack](id){
        return this.objects[id]
    }
    static unpack(obj){
        for(const field in obj){
            const handler = this[fld[field]]?.[fn.unpack]
            if (handler){obj[field] = handler(obj[field],field,obj)}
        }
        obj.__proto__ = this.prototype; 
        return obj
    }
    pack(){
        const cls = this.constructor
        const obj = {}
        for(const prop of Object.getOwnPropertyNames(this)){
            const handler = cls[fld[prop]]?.[fn.pack]
            if (handler){obj[prop] = handler(this[prop],prop,this)}
            else {obj[prop] = this[prop]}
        }
        return obj
    }
    static async init(){
        let {tableName:name,pkName:pk,dbName:db} = this
        const dbData = JSON.parse(localStorage.idb||"{}")
        if(!(db in dbData)){dbData[db] = {v:1,tables:{},name:db}}
        this.db = db = dbData[db]
        if(!db.tables[name]){
            db.tables[name] = {pk,name:name}
            const req = indexedDB.open(db.name,db.v++)
            req.onupgradeneeded = (e)=>{
                const db = req.result;
                db.createObjectStore(name,{keyPath:pk,autoIncrement:true});
            }
            await new Promise(o=>req.onsuccess = o)
        }
        // TODO check for key
        localStorage.idb = JSON.stringify(dbData)
    }
    static put(objs){
        if(!this.db){throw new Error(`Table ${this.tableName} is not initialized`)}
        if(!Array.isArray(objs)){objs = [objs]}
        return new Promise((ok,err)=>{
            const req = indexedDB.open(this.db.name,this.db.v);
            req.onsuccess = (e)=>{
                const db = req.result;
                const tx = db.transaction(this.tableName,"readwrite");
                objs.forEach(obj=>tx.objectStore(this.tableName).put(obj).onsuccess = (e)=>obj[this.pkName] = e.target.result)
                tx.oncomplete = ok
                tx.onerror = err
            }
            req.onerror = err
        })
    }
    static delete(keys){
        if(!Array.isArray(keys)){keys = [keys]}
        keys = keys.map(k=>Number(k))
        console.log(keys)
        return this.iter((val,cs)=>{
            if(keys.includes(cs.key)){
                console.dir(cs.key)
                cs.delete()
            }else{console.log(cs.key)}
        },true)
    }
    static get(id){
        return new Promise((ok,err)=>{
            const req = indexedDB.open(this.db.name,this.db.v);
            req.onsuccess = (e)=>{
                const db = req.result;
                const store = db.transaction(this.tableName).objectStore(this.tableName)
                if(id){
                    const cmd = store.get(id);
                    cmd.onsuccess = ()=>ok(cmd.result)
                    cmd.onerror = err
                }else{
                    const cmd = store.getAll();
                    cmd.onsuccess = ()=>ok(cmd.result)
                    cmd.onerror = err
                }
            }
            req.onerror = err
        })
    }
    static iter(cb,write=false){
        return new Promise((ok,err)=>{
            const ret = {}
            const req = indexedDB.open(this.db.name,this.db.v);
            req.onsuccess = (e)=>{
                const db = req.result;
                const crsReq = db.transaction(this.tableName,write?"readwrite":"readonly").objectStore(this.tableName).openCursor()
                crsReq.onsuccess = (e)=>{
                    const crs = e.target.result;
                    if(crs){cb(crs.value,crs);crs.continue()}else{ok()}
                }
                crsReq.onerror = err
            }
            req.onerror = err
        })
    }
    static async select(cb){
        const arr = []
        await this.iter(e=>cb(e) && arr.push(e))
        return arr
    }
}
export class VueStorage extends IDBStorage{
    static async init(){
        if(this.db){return}
        if(this.objects===undefined){this.objects = reactive({})}
        await super.init();
        const objects = {}
        await this.iter((obj)=>{objects[obj[this.pkName]] = this.unpack(obj)})
        Object.assign(this.objects,objects)
        const Storage = this
        watch(this.objects,()=>{},{
            deep:true,
            onTrigger(e){
                console.log(e)
                if(e.target instanceof Storage){Storage.put(e.target)}
                else if(e.type == "delete"){Storage.delete(e.key)}
            }
        })
    }
    static get(id){return id==null?Object.values(this.objects):this.objects[id]}
    static async put(objs){
        if(!Array.isArray(objs)){objs = [objs]}
        const packed = objs.map(obj=>obj.pack())
        await super.put(packed);
        objs.forEach((obj,i)=>{
            if(obj[this.pkName]==null){
                const pk = obj[this.pkName] = packed[i][this.pkName]
                this.objects[pk] = obj
            }
        })
        super.put(objs)
    }
}
export class Model{
    static vals(){return Object.values(this.objects)}
    static provideId(id){
        return id in this.objects?this.objects[id]:(this.objects[id] = Object.assign(new this(id),{id}))
    }
    // remote Functions
    // Events
    //static [on.bindNS](cls){cls.objects = {}}
    // Serialization
    static [cfg.fromJSON](data){
        let {fields,values,...other} = data;
        Object.assign(this,other)
        if(!values){return}
        for(const [id,arr] of Object.entries(values)){
            if(arr==null){delete this.objects[id];continue}
            const obj = {}
            for(const [i,val] of Object.entries(arr)){
                const field = fields[i]
                const ftype = this.prototype[fld[field]]
                if(ftype?.[fn.parseField]){
                    const parser = ftype[fn.parseField]
                    obj[field] = parser.apply(ftype,[val,this,field])
                }else{
                    obj[field] = val
                }
            }
            Object.assign(this.provideId(id),obj)
        }
    }
    static [fn.parseField](val){return this.provideId(val)}
    static [fn.dumpField](val){return val.id}
}
// serialization
function processJSON(data,ns){
    if(data!=null && typeof data =="object"){
        for(const key of Object.keys(data)){
            data[key] = processJSON(data[key],ns)
        }
        if(data[ "$t" ]){
            const type = ns[data.$t]
            if(!type||!type[cfg.fromJSON]){
                throw Error(`${data.$t} type or his 'fromJSON' handler wasnt found`)}
            delete data.$t
            return type[cfg.fromJSON](data)
        }
    }
    return data
}
export function toJSON(obj){
    const ret = {}
    for(const [key,val] of Object.entries(obj)){
        const fieldDescriber = obj[fld[key]]
        if(fieldDescriber?.[fn.dumpField]){
            ret[key] = fieldDescriber[fn.dumpField](val,obj,key)
        }
        // if val is not Object
        else if(val==null || !['object','function'].includes(typeof val)){
            ret[key] = val
        }
    }
    return ret
}
// types
export const jsonField = new Object({
    [fn.parseField](val){ return JSON.parse(val)},
    [fn.dumpField](val){ return JSON.stringify(val)}
})
export const dateField = new Object({
    [fn.parseField](val){ return val && new Date(val)},
    [fn.dumpField](val){return val&&val.toISOString().substr(0,10)}
})
function remote(tg,key,{val:cb,isProto}){// TODO
    tg[key] = async function(args){
        const type = isProto?this.constructor:this
        const src = this[cfg.ns][cfg.src]
        if(isProto){args = [this.id,args]}
        const req = await fetch(`${src}/${type[cfg.name]}/${key}`,{method:'POST',body:JSON.stringify(args)})
        let result = await req.json()
        result = this[cfg.ns][fn.processJSON](result)
        if(cb){
            const modifed = cb.apply(this,[result])
            if(modifed!=undefined){result = modifed}
        }
        return result
    }
}
