const scopes = {}
export const ssn = Symbol('SymbolScopeName')
export const fn = SymbolScope('function')
export const smb = SymbolScope('SymbolConfig')
export const cfg = SymbolScope('config')
export const DEFAULT = Symbol('default')
export const update = fn.extend;
//
export const get = SymbolScope('getter')
export const set = SymbolScope('getter')
export const add = SymbolScope('add')
export const prx = SymbolScope('proxy')
export function fromString(s){
    const [scope,...names] = s.split(':')
    if(!names.length){return s}
    return scopes[scope]?.[fromString(names.join(':'))]
}
const symbolParams = {}
export function SymbolScope(name){
    const cache = {[ssn]:name}
    const proxy = new Proxy(()=>{},{
        get(fn,key,r){
            return cache[key]||(cache[key] = Symbol(`${name}:${key.description||key}`))
        },
        set(tg,key,val){cache[key]=val;return true},
        apply(tg,_,args){
            const key = Symbol('SymbolParams')
            symbolParams[key] = [name,args]
            return key;
        }
    })
    return scopes[name] = proxy
}
export function getScope(scope,obj,deep=true){const name = scope[ssn]
    return Object.fromEntries([...iterSymbols(obj,deep)].filter(e=>e[0]==name).map(e=>[e[1],e[2]]))
}
export function toScope(scope,obj){
    return obj[fn.map](([k,v])=>[scope[k],v])
}
export function processSymbol(symbol,externContext){
    if(typeof symbol !="symbol"){return}
    let [scope,...args] = symbol.description.split(':')
    if(!args.length && scope=="SymbolParams"){
       [scope,args] = symbolParams[symbol]
       if(!scope){return}
    }else{
        args = [fromString(args.join(':'))]
    }
    const {target} = externContext
    let handler = (typeof symbol == 'symbol') && target[smb[symbol]] // symbolHandler
    if(!handler){handler = target[smb[scope]]} // scopeHandler
    if(handler){
        const localContext = {symbol,scope,args,value:target[symbol]}
        if(handler(localContext,externContext)){
            delete target[symbol];
            if(scope=='SymbolParams'){
                delete symbolParams[symbol];}
        }
    }
}
export function *iterSymbols(obj,deep=true){
    let current = obj
    while(current){
        for(const symbol of Object.getOwnPropertySymbols(current)){
            if(current.__proto__?.[symbol]!=undefined && !deep){continue}
            const [scope,name] = symbol.description.split(':')
            yield [scope,name,obj[symbol],current]
        }
        current = deep? current.__proto__:null
    }
}
export function Proxymbol(obj){
    return new Proxy(obj,{
        get(tg,key,prox){
            if(key==prx.on){return true}
            const propHandler = tg[prx[get[key]]];
            if(propHandler){return propHandler(tg,key,prox);}
            else{const getHandler = tg[prx.get];
                if(getHandler){return getHandler(tg,key,prox);}
                else { return Reflect.get(tg,key,prox) } 
            }
        },
        set(tg,key,val,prox){
            const propHandler = tg[prx[set[key]]];
            if(propHandler){return propHandler(tg,key,val,prox);}
            else{const setHandler = tg[prx.set];
                if(setHandler){return setHandler(tg,key,val,prox);}
                else { return Reflect.set(tg,key,val,prox) } 
            }
        }
    })
}
const gOPS = Object.getOwnPropertySymbols;
export function RelationSystem(name,to=Object.prototype){
    const relsys = new SymbolScope('rel.'+name)
    Object[update]({},{
        [ relsys.path](){
            const parentPath = this[relsys.parent]?.[relsys.path]()
            return (parentPath?parentPath+'/':'')  +(this[relsys.name]||"")
        },
        [relsys.add](childs){
            if(!gOPS(this).includes(relsys.childs)){this[relsys.childs] = {}}
            for(const [name,child] of Object.entries(childs)){
                child[relsys.name] = name;
                if(!child[relsys.parent]){child[relsys.parent] = this};
                this[relsys.childs][name] = this[relsys.adder] ? this[relsys.adder](name,child) : child 
            }
        },
        [relsys.get](name){return this[relsys.childs][name]||this[relsys.getter]?.(name)},
        [smb[relsys.childs]]({value:children},{target}){
            target[relsys.add](children);
            return true;
        },
        [smb['rel.'+name]]({args:[name],value},{target}){
            if(Array.isArray(name)){
                name = name[0]
                target[name] = value
                target[relsys.add]({[name]:value})
                return true
            }
        }
    })
    relsys[update]({
        new(name,childs={},parent=null,base={}){
            Object.assign(base,{[relsys.name]:name,[relsys.parent]:parent})
            base[relsys.add](childs)
            return base
        }
    })
    return relsys
}
export default function Confix(...objs){
    for(const i in objs){
        const obj = objs[i]
        if(obj==null){conti}
        const context = {target:obj}
        for(const symbol of Object.getOwnPropertySymbols(obj)){processSymbol(symbol,context)}
        if(typeof obj == 'function' && obj.prototype){
            const protoContext = {target:obj.prototype} 
            for(const symbol of Object.getOwnPropertySymbols(obj.prototype)){processSymbol(symbol,protoContext)}
        }
        objs[i] = context.target
    }
    return objs.length>1?objs:objs[0]
}
// tools
Object.prototype[fn.extend] = function(obj,proto){
    if(obj==null){return}
    Object.assign(this,obj);
    const context = {target:this}
    for(const key of Object.getOwnPropertySymbols(obj)){processSymbol(key,context)}
    if(proto){
        const instProto = this.prototype
        Object.assign(instProto,proto)
        const protoContext = {target:instProto}
        for(const name of Object.keys(proto)){processSymbol(name,context)}
    }
    return context.target
}
Object[update]({},{
    [fn.map](cb){return Object.fromEntries(Object.entries(this).map(cb))},
    [fn.vmap](fn){return Object.fromEntries(Object.entries(this).map(([k,v],i)=>[k,fn(v,k,i)]))},
    [fn.filter](cb){return Object.fromEntries(Object.entries(this).filter(cb))},
    [fn.vfilter](fn){return Object.fromEntries(Object.entries(this).filter(([k,v],i)=>fn(v,k,i)))},
    [fn.empty](){return Object.keys(this).length==0},
    [fn.new](...args){
        const ctx = {inst: new this(...args),args}
        const ctrs = ctr('in',this).sort( (a,b)=>a[0]>b[0]?1:-1 ).forEach(e=>e[1].apply(ctx.inst,[ctx]))
        return ctx.inst
    },
    [smb.getter]({args:[key],value:val},{target:tg}){
        const desc = Object.getOwnPropertyDescriptor(tg,key)
        Object.defineProperty(tg,key,{get:val,set:desc?.set})
    },
    [smb.setter]({args:[key],value:val},{target:tg}){
        const desc = Object.getOwnPropertyDescriptor(tg,key)
        Object.defineProperty(tg,key,{set:val,get:desc?.get})
    },
    [smb.add]({args:[name,...args],value},{target}){
        let handler = target[name]?.[fn.add]
        if(!handler){handler = value?.[fn.add]}
        if(handler){
            handler(target,name,value,...args)
        }else{target[name]!=null?target[name]+=value:target[name]=value}
        return true
    },
    [smb[prx.on]](_,ext){ext.target = Proxymbol(ext.target);return true} // inline make Proxy
})