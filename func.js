import { surl } from ".";
import { update,cfg,fn,smb } from "./symbol";
function copy(){
    return SharedFunc(this.funcs,this.context,this.key)
}
export function SharedFunc(funcs=[],context={},key=Symbol('SharedFunc')){
    function func(...args){
        const queue = [...func.funcs]
        const ctx = {args,self:this,__proto__:func.context,queue}
        let handler = queue.shift()
        while(handler && !ctx.break){
            ctx.handler = handler;
            const handlerFn = handler[func.key]||handler
            const isNative = handlerFn[cfg.isNative]
            const result = isNative?handlerFn.apply(this,args):handlerFn.apply(ctx,args)
            if(result != undefined){ctx.return = result}
            handler = queue.shift()
        }
        return ctx.return
    }
    return func[update]({context,funcs,key,copy})
}
Function[update]({},{
    [fn.add](tg,key,addVal){
        const val = tg[key];
        if(!val){tg[key] = SharedFunc([addVal])
        }else if(val.funcs){// val is SharedFunc
            val.funcs.push(addVal)
        }else{
            val[cfg.isNative] = true
            tg[key] = SharedFunc([addVal,val])
        }
    }
})
export function remoteFunc(){
    const shared = SharedFunc()
    async function remote(){
        const url = shared[surl.path]()
        const body = JSON.stringify(this.args)
        const req = await fetch(url,{method:'POST',body})
        return await req.json()
    }
    shared.funcs.push(remote)
    return shared
}