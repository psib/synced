import {e as $e$,gui,bind,List, CSS,css} from "../render"
import { ObjectStorage,Proxymbol,prx,cfg,fn} from "../index";
import Confix from "../symbol";
// Read list from IndexedDB
const TODO = new ObjectStorage("TODO");
TODO.init().then(async ()=>{
    TodoList.update(await TODO.get())
});
Object[fn.extend]({},{
    [prx.get](tg,key){
        const el = h.current;
        if(el){
            if(!tg.hasOwnProperty(cfg.deps)){tg[cfg.deps] = {}}
            const deps = tg[cfg.deps]
            if(!deps[key]){deps[key]=[]}
            deps[key].push(el);
        }
        return tg[key]
    },
    [prx.set](tg,key,val){
        tg[key] = val;
        const deps = tg[cfg.deps]?.[key]
        if(deps){
            deps.forEach(h.update);
            tg[cfg.deps][key] = []
        }
        return true;
    }
})
const obj = Proxymbol({
    name:"Paul",
    age:20
})
// Render TODO List
window.list = TodoList;
const App = ()=><div id="app"> 
    <button onClick={()=>console.log("Hello")}>Hello</button>
    <br/>{obj.name} {obj.age}
</div>;
App[fn.extend]({
    [css.style](){
        return {
            fontFamily: 'Avenir, Helvetica, Arial, sans-serif',
            '-webkit-font-smoothing': 'antialiased',
            '-moz-osx-font-smoothing': 'grayscale',
            textAlign: 'center',
            color: '#222',
            marginTop: '60px',
        }
    }
})
// style
CSS.main.push({
  "#app":App
})
document.head.append(<CSS.main/>)
document.body.append(<App/>)
