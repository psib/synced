import Confix,{SymbolScope,RelationSystem,cfg,fn,get} from "./symbol.js"
export const ctr = SymbolScope('constructor')
export const on = SymbolScope('event')
export * from "./symbol.js"
export * from "./func.js"
export * from "./data.js"

export {Confix}
// tools
export function newClass(name,base){
    const classGener = new Function('ext',`return class ${name} extends ext{}`)
    return classGener(base)
}
// relations
export const surl = RelationSystem('serverURL')
export const curl = RelationSystem('URL')
Object[fn.extend]({},{
    [get[ curl.wrapper ]](){return this[curl.parent]}
})
//global.window?.[fn.extend]({curl,surl,cfg})
