import { h, withCtx } from "vue";

export class CSS{
    dict = {}
    constructor(name=""){
        this.name = name;
    }
    /**@param {Object.<string,CSSStyleDeclaration>} dict */
    push(...dicts){
        for(let dict of dicts){
            if(dict[css.css]){dict = dict[css.css](this)}
            for(const name in dict){
                if(!this.dict[name]){this.dict[name] = {}}
                const cuurrDef = this.dict[name];// current CSS Definition
                let newDef = dict[name];
                if(newDef[css.style]){newDef = newDef[css.style](this,name)}
                for(const prop in newDef){
                    let value = newDef[prop];
                    if(value[css[prop]]){value = value[css[prop]](cuurrDef,prop,this,name)}
                    if(value!=undefined){cuurrDef[prop] = value;}
                }
            }
        }
    }
}
CSS.main = new CSS('main')
/**@param {CSSStyleDeclaration} obj*/
function obj2css(obj){
    const el = document.createElement('div')
    for(const attr in obj){el.style[attr] = obj[attr]}
    return el.style.cssText
}
export let defShadow = "2px 2px 10px grey"// "7px 7px grey";
export const shortcuts = {
    pa:"padding",
    px:["paddingLeft","paddingRight"],
    py:["paddingTop","paddingBottom"],
    pl:"paddingLeft",
    pr:"paddingRight",
    pt:"paddingTop",
    pb:"paddingBottom",

    ma:"margin",
    mx:["marginLeft","marginRight"],
    my:["marginTop","marginBottom"],
    ml:"marginLeft",
    mr:"marginRight",
    mt:"marginTop",
    mb:"marginBottom",

    b:"border",
    bc:"borderColor",
    br:"borderRadius",
    bw:"borderWidth",

    f:"font",
    fs:"fontSize",
    fw:"fontWeight",
    ff:"fontFamily",
    fc:"color",
    fh:'lineHeight',

    w:"width",
    wn:"minWidth",
    wx:"maxWidth",

    h :"height",
    hn :"minHeight",
    hx :"maxHeight",

    oa :"overflow",
    ox :"overflowX",
    oy :"overflowY",

    ps:"position",
    dp:"display",
    bg:"backgroundColor",
    sh(v){return {boxShadow:v==""?defShadow:v}},
}
export function shortCSS(ctx){
    /**@type {CSSStyleDeclaration} */
    const style = {};
    for(const key in ctx){
        if(key in shortcuts){
            const val = shortcuts[key]
            switch(typeof val){
                case "string":style[val] = ctx[key];break;
                case "object":val.forEach(e=>style[e]=ctx[key]);break;
                case "function":Object.assign(style,val(ctx[key]))
            }
        }
    }
    return style;
}